#include "Header.h"

int main()
{
	//Instantiating the Interface object.
	Interface* newInterface = new Interface();
	Logic* newLogic = new Logic();


#pragma region variables
	int userInput;
	//hold user menu responce -> used in Interface::getInput by variable
	string menuOptionMainGuide = 
		" Infix to Postfix converter by SID: 1309759" 
		"\n\n"
		" Please make your selection: \n \n";
	string menuOptionOne = "	(1) Convert Infix to Postfix. \n";
	string menuOptionSecond = "	(2) About.\n";
	string menuOptionThird = "	(3) Exit.\n \n";
	string menuOptionForth = "	Selection: ";
#pragma endregion variables

	//Do Loop wrapping main menu until user input; exit == 3
	do {

		//int used in switch statement; default == 0.
		int choice = 0;
		
//Construction of the user interface.
#pragma region MenuCreation
		newInterface->MenuConstructor(menuOptionMainGuide);
		newInterface->MenuConstructor(menuOptionOne);
		newInterface->MenuConstructor(menuOptionSecond);
		newInterface->MenuConstructor(menuOptionThird);
		newInterface->MenuConstructor(menuOptionForth);
#pragma endregion MenuCreation 
		
		cin >> choice;
		userInput = newInterface->GetInput(choice);

//strings cannot be created inside the switch; infix + postfix used in case 1
#pragma region strings
		string infix;
		string postfixString;
#pragma endregion strings

		//switch menu
		switch (userInput) {

		//Converting Infix to Postfix
		case 1:
			
			system("cls");
			newInterface->MenuConstructor(" CONVERT INFIX TO POSTFIX. \n");
			newInterface->MenuConstructor("\n	Enter Infix Expression: ");
			cin >> infix;
			cout << "	infix = " << infix << "\n";
			postfixString = newLogic->ConvertToPostfix(infix);
			cout << "	postfix = " << postfixString << "\n\n";
			
			system("PAUSE");

			system("cls");
			break;

		//About page
		case 2:
			system("cls");
			newInterface->MenuConstructor(" ABOUT PAGE. \n\n");
			newInterface->MenuConstructor("	Software developed by SID: 1309759 for MOD003197. \n ");
			newInterface->MenuConstructor("	Last modified: 02 May 16. \n \n");

			system("PAUSE");
			system("cls");
			break;

		//Exit + exit message
		case 3:
			system("cls");
			newInterface->MenuConstructor(" EXIT PAGE. \n\n");
			newInterface->MenuConstructor("	See you soon (: Thank you for using this program.\n \n");

			newLogic->~Logic();
			newInterface->~Interface();
			system("PAUSE");
			break;
		}
	} while (userInput != 3); // 3 == Closes the program.

	return 0;//Main return
}//end of main