#pragma region declarations

#include <iostream>
#include <stack> //Allows the use of std::stack
#include <string> 

using namespace std;

#ifndef INFIXTOPOSTFIX_H
#define INFIXTOPOSTFIX_H
#pragma endregion declarations

//A class that holds all user interface methods.
class Interface {
public:
	int userInput;
	//function that captures the user's input
	int GetInput(int);
	void MenuConstructor(string option);
	~Interface(void);
};

class Logic {
public:
	// Function to convert Infix expression to postfix 
	string ConvertToPostfix(string);
	// Checks if input is letter or digit. Only Single characters are legal.
	bool IsOperand(char);
	// Function to verify whether a character is operator symbol or not. 
	bool IsOperator(char);
	// Function to verify if association is right. 
	bool IsRightAssociative(char);
	//Calculates the weight of an operator.
	int Precedence(char);
	// Function to verify whether an operator has higher precedence over other
	int HasHigherPrecedence(char, char);
	~Logic(void);
};
#endif