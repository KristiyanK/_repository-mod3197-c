//citytest2_Array of char.cpp
#pragma region Include/namespace
#include <iostream>
#include <cstdlib>
#include <cstring>


using namespace std;
#pragma endregion

class City
{
    char name[12];
public:
    City( char input[] )
    {
        strcpy_s( name, input );
    }

    char* getName()
    {
        return name;
    }
};//end of City

class Population
{
	char size[4];
public:
	Population(char input[])
	{
	strcpy_s( size, input );
	}
	
	char* getPoplation()
	{
		return size;
	}
};//end of population


int main( void )
{
    City acity( "Cambridge" );
    cout << "City is: " << acity.getName() << endl;	
	Population size ("30");
	cout << "City's size is: " << size.getPoplation() << endl;
    system("PAUSE");
    return 0;
}

