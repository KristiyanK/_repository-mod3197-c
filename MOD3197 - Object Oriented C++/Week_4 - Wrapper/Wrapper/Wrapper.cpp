/* Wrapper.cpp */
#include<cstdlib>
#include <iostream>

using namespace std;

class WrapInt
{
	int value;
	char digitString[3];

public:
	WrapInt ( int val )
	{
		value = val;					// sets first attribute
		int tens = value/10;			// following lines set second attribute
		int ones = value%10;			// modulus used to return remainder of division
		int asciiTens = tens + 48;
		int asciiOnes = ones + 48;
		digitString[0] = asciiTens;
		digitString[1] = asciiOnes;
		digitString[2] = '\0';
	}

	int getInt()
	{
		return value;
	}

	char* getString()
	{
		return digitString;
	}

};

int main ( void )
{
	int value = 69;
	WrapInt test ( value );
	cout << "Value is: " << test.getInt() << endl << "String is: " << test.getString() << endl;
	system ("PAUSE");
	return 0;
}