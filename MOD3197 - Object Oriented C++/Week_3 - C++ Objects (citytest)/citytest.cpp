// citytest.cpp
#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

class City
{
    char name[12];
public:
    City( char input[] )
    {
        strcpy_s( name, input );
    }

    char* getName()
    {
        return name;
    }    
};

int main( void )
{
    City acity( "Cambridge" );
    cout << "City is: " << acity.getName() << endl;	
	
    system("PAUSE");
    return 0;
}
