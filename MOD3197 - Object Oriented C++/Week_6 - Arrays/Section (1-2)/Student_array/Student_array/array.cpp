// Example C++ Arrays of students
#include <iostream>
#include <cstdlib>
#include <cstring>
#include "array.h"

using namespace std;

class Student
{
	char name[10];			// details of student
	char studID[8];

public:
	Student(char* name, char* studID) // constructorb
	{
		strcpy_s(this->name, name);
		strcpy_s(this->studID, studID);
	}

	char* getName()   // returns name
	{
		return name;
	}

	char* getID()     // returns ID
	{
		return studID;
	}
};

class StudentList
{
	Student* group[10];  // space for 10 students

public:

	StudentList()            // populate list - better from file
	{
		group[0] = new Student("Mary", "0101111");
		group[1] = new Student("Jill", "0101112");
		group[2] = new Student("Bill", "0101145");
		group[3] = new Student("Mohammed", "0221341");
		group[4] = new Student("Tom", "0213213");
		group[5] = new Student("Mike", "9987654");
		group[6] = new Student("Vijay", "9899872");
		group[7] = new Student("Shona", "0023451");
		group[8] = new Student("Maria", "0200022");
		group[9] = new Student("Tony", "9900123");
	}

	Student* findStudent(char* studID) // search given ID. returns null if not found 
	{
		Student* theStudent = NULL;  // for the student found
		int id = 0;   // index for search

		while (id <= 9 && theStudent == NULL)   // until whole array  or student found.
		{
			if (strcmp(studID, group[id]->getID()) == 0)  // check studID against array
			{
				theStudent = group[id];
			}
			id++;
		}
		return theStudent;  // either student found or null
	}
};

int main(void)
{
	StudentList studentSearch;
	char userInput[8];
	cout << "Please insert Student ID:" << endl;
	cin >> userInput;
	cout << "Student: " << studentSearch.findStudent(userInput) << endl;

	system("PAUSE");
	return 0;

}//end of main