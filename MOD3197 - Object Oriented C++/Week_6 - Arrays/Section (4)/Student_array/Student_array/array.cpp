//Header file that hold onyl the class declarations, standard library include and namespace statements;
#pragma region includes/namespace
#include "array.h"

using namespace std;

#pragma endregion includes/namespace

Student::Student(char* name, char* studID) // constructorb
	{
		strcpy_s(this->name, name);
		strcpy_s(this->studID, studID);
	}

char* Student::getName()   // returns name
	{
		return name;
	}

char* Student::getID()     // returns ID
	{
		return studID;
	}


StudentList::StudentList()            // populate list - better from file
	{
		group[0] = new Student("Mary", "0101111");
		group[1] = new Student("Jill", "0101112");
		group[2] = new Student("Bill", "0101145");
		group[3] = new Student("Mohammed", "0221341");
		group[4] = new Student("Tom", "0213213");
		group[5] = new Student("Mike", "9987654");
		group[6] = new Student("Vijay", "9899872");
		group[7] = new Student("Shona", "0023451");
		group[8] = new Student("Maria", "0200022");
		group[9] = new Student("Tony", "9900123");
	}

Student* StudentList::findStudent(char* studID) // search given ID. returns null if not found 
	{
		Student* theStudent = NULL;  // for the student found
		int id = 0;   // index for search

		while (id <= 9 && theStudent == NULL)   // until whole array  or student found.
		{
			if (strcmp(studID, group[id]->getID()) == 0)  // check studID against array
			{
				theStudent = group[id];
			}
			id++;
		}
		return theStudent;  // either student found or null
	}