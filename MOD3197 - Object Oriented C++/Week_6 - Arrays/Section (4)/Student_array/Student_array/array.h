//Header file that hold onyl the class declarations, standard library include and namespace statements;
#ifndef ARRAY_H
#define ARRAY_H

#pragma region includes/namespace
#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

#pragma endregion includes/namespace

class Student
{
	char name[10];			// details of student
	char studID[8];

public:
	Student(char* name, char* studID); // constructorb
	char* getName();   // returns name
	char* getID();     // returns ID
};


class StudentList
{
	Student* group[10];  // space for 10 students

public:
	StudentList();            // populate list - better from file
	Student* findStudent(char* studID); // search given ID. returns null if not found 
};
#endif