/* calcsum.c - Updated */

#pragma region Includes
#include <iostream>
//Library allowing the 'system' command in main
#include <stdlib.h>
#pragma endregion

#pragma region Global variables
char userInput;
float a, answer;
#pragma endregion

float FtoC( float val )
{
   float CelsiusValue;
   CelsiusValue = (a - 32) * 5 / 9;
   return CelsiusValue;
};

float CtoF(float val) {
	float fahrenheitValue;
	fahrenheitValue = a * 9 / 5 + 32;
	return fahrenheitValue;
}

int GetInput()
{
	int userInput;
	std::cin >> userInput;
	return userInput;
}

void CalculatorOptions() {
	std::cout << "Please choose between the two options:\n";
	std::cout << "Please make your selection \n \n";
	std::cout << "	(1) Convert from Celsius to Fahrenheit.\n";
	std::cout << "	(2) Convert from Fahrenheit to Celsius.\n";
	std::cout << "	(3) Exit. \n \n";
	std::cout << "Selection: ";
}


/* Main function */
int main(void)
{
	int choice = 0;

	//Loops inside the menu until user input is 1 or 2;
	do {
		//Clears the console in case it needs to loop back into the main menu
		system("cls");

		//Displays the menu options
		CalculatorOptions();
		choice = GetInput();
		switch (choice) {

		//Converts from Fahrenheit to Celsius
		case 1:
			//Console.clear
			system("cls");
			std::cout << "Please enter the desired Fahrenheit value to be converted into Celsius \n";
			std::cout << "\n Value: ";
			std::cin >> a;
			answer = FtoC(a);
			std::cout << "\n" << a << " Fahrenheit is " << answer << " in Celsius." << "\n";
			system("PAUSE");
			break;

		//Converts from Celsius to Fahrenheit
		case 2:
			//Console.clear
			system("cls");
			std::cout << "Please enter the desired Fahrenheit value to be converted into Celsius \n";
			std::cout << "\n Value: ";
			std::cin >> a;
			answer = CtoF(a);
			std::cout << "\n" << a << " Fahrenheit is " << answer << " in Celsius." << "\n";
			system("PAUSE");
			break;
		
		//Closes the program
		case 3:
			system("cls");
			std::cout << "Thank you for using this converter by Kristiyan Kovachev \n";
			system("PAUSE");
			//Closes the program.
			std::terminate();
			break;
		}
	} while (userInput != 3);

   return 0; //Main return
}//end of main