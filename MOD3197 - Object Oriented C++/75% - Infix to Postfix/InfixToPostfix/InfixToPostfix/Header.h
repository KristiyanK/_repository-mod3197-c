#pragma region declarations

#include <iostream>
#include <stack> //Allows the use of std::stack
#include <string> 

using namespace std;

#ifndef INFIXTOPOSTFIX_H
#define INFIXTOPOSTFIX_H

#pragma endregion declarations
class Process {

public:
// Function to convert Infix expression to postfix 
string InfixToPostfix(string str);

// Function to verify whether an operator has higher precedence over other
int HasHigherPrecedence(char op1, char op2);

// Function to verify whether a character is operator symbol or not. 
bool IsOperator(char C);

// Function to verify whether a character is alphanumeric chanaracter (letter or numeric digit) or not. 
bool IsOperand(char C);

bool IsRightAssociative(char op);

int GetOperatorWeight(char op);

};
#endif