#include "Header.h"

int main(void)
{
	string fraction1;
	cout << "This program processes a fraction entered at the keyboard using syntax 'a/b'." << endl;
	cout << "Enter fraction 1:";
	cin >> fraction1;
	int num = readNum(fraction1);
	int den = readDen(fraction1);
	Fraction f1(num, den);   // Create the first fraction object
	
	cout << "Fraction1 is " << f1.getNumerator() << "/" << f1.getDenominator() << endl;

	return 0;
}