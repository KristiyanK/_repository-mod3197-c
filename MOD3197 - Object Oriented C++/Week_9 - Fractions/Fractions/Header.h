#include <iostream>
#include <string>
using namespace std;

// This class stores a fraction's numerator and denominator
class Fraction
{
	int numerator;
	int denominator;
public:
	Fraction() { } // Empty constructor required because no arg constructor called in main for f3
	Fraction(int a, int b)
	{
		numerator = a;
		denominator = b;
	}

	int getNumerator(void) { return numerator; }
	int getDenominator(void) { return denominator; }
	void setNumerator(int x) { numerator = x; }
	void setDenominator(int y) { denominator = y; }
};

// The following two functions are 'global functions',
// ie not a member of a class - not an OO approach but common C-like approach.
// Assumes inputs are valid (no input error checking).
int readNum(string str)
{
    int fslash = str.find('/'); // Finds index of '/' symbol in string
	string val = str.substr(0,fslash); // Extracts numerator as a string
    int value = atoi( val.c_str() ); // Converts numerator to an int
    return value;
};

int readDen(string str)
{
    int fslash = str.find('/');
	int end = str.length(); // Length of input string
	string val = str.substr(fslash+1, end); // Extracts denominator as a string
	int value = atoi( val.c_str() );  // Converts denominator to an int
    return value;
};
