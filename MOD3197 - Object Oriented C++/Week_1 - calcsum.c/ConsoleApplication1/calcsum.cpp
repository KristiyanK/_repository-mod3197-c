/* calcsum.c */
#include <iostream>

float timestwo( float val )
{
   float twice;
   twice = 2.0 * val;
   return twice;
};

/* Main function */
int main(void)
{
   float a, answer;
   std::cout << "Enter a value > "; 
   std::cin >> a;
   answer = timestwo(a);
   std::cout << "The answer is> " << answer << ".\n";
   system("PAUSE");
   return 0;
}