#pragma region includes
#include "ListofTowns.h"
#pragma endregion

int main()
{
#pragma region variables
	//town names variables.
	string firstTown = "Cambridge";
	string secondTown = "London";
	string thirdTown = "Manchester";
	string forthTown = "Birmingham";
#pragma endregion

	//Creating new object to hold new town list values
	TownList* townList = new TownList();

	//Adding towns to list
	townList->addFirstTown(firstTown);
	townList->addTownAtEnd(secondTown);
	townList->addTownAtEnd(thirdTown);
	townList->addTownAtEnd(forthTown);

	townList->addTownAtStart("Ruse");

	//Listing all towns
	townList->listAllNames();


	system("pause");
}