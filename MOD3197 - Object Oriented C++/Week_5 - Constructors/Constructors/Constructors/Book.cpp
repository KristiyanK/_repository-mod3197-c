#include <iostream>
#include <string>
#include <cstdlib>

class Book{
private:
	int* pages;
public:
	Book() {}
	Book(int num) { setPages(num); }
	Book(const Book& copy) 
		: pages(new int (*copy.pages)) {}

	~Book() {
		std::cout << "destroying object\n";
	}

	void setPages(int num) {
		pages = &num;
		std::cout << "Set pages is b1 " << num  << "\n";
		std::cout << "Set pages is b1adasd " << *pages  << "\n";
}	
	int getPages() {
		std::cout << "get pages " << *pages << "\n";
		return *pages;
	
	}
};

void main() {
	Book b1;
	int variable = 500;
	int* variableaddress;
	std::cout << "Variable Address" << &variable << "\n";
	variableaddress = &variable;
	std::cout << "Variable Dereference" << *variableaddress << "\n";

	b1.setPages(variable);
	std::cout << "Number of pages is b1 " << b1.getPages() << "\n";

	Book b2(300);
	std::cout << "Number of pages is b2 " << b2.getPages() << "\n";

	Book b3(b1);
	b3.setPages(200);
	std::cout << "Number of pages is b3 " << b3.getPages() << "\n";

	system("pause");
} 