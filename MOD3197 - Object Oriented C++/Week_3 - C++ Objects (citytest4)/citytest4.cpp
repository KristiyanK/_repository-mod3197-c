//citytest4
#pragma region Include/namespace
#include <iostream>
#include <cstdlib>
#include <cstring>


using namespace std;
#pragma endregion

class City
{
    char name[12];
	int popn;
public:
    City( char input[])
    {
        strcpy_s( name, input );
    }

    char* getName()
    {
        return name;
    }

	void setPopn(int size)
	{
		popn = size;	
	}

	int getPopn()
	{
		return popn;
	}
};//end of Object: City


int main( void )
{
    City* acity = new City( "Cambridge");
	(*acity).setPopn(3000000);
    cout << "City is: " << (*acity).getName() << endl;
	cout << "City's population is: " << acity->getPopn() << endl;

	City* bcity = new City("Ipswich");
	bcity->setPopn(120000);
	cout << "City is: " << bcity->getName()  << endl;
	cout << "City's population is: " << bcity->getPopn()  << endl;

    system("PAUSE");
    return 0;
}//end of main
