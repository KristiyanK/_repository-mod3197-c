#include "vehicle.h"

void main()
{
	Vehicle newVehicle(4, 2.5);
	cout <<"Vehicle.weight " << newVehicle.getweight()<<endl;
	cout <<"Vehicle.num wheels " << newVehicle.getwheels()<<endl;
	newVehicle.service();

	Car* newCar = new Car(4, 3.0f, 2);
	cout <<"CAR.weight " << newCar->getweight()<<endl;
	cout <<"CAR.num wheels " << newCar->getwheels()<<endl;
	cout <<"CAR.passengers " << newCar->getpassengers()<<endl;
	newCar->service();

	Lorry* newLorry = new Lorry(4, 2.0f, 3.0f);
	cout <<"Lorry.weight " << newLorry->getweight()<<endl;
	cout <<"Lorry.num wheels " << newLorry->getwheels()<<endl;
	cout <<"Lorry.height " << newLorry->getHeight()<<endl;
	newLorry->service();

	system("pause");
}