// passbyreference.cpp
#include <iostream>
#include <cstdlib>

using namespace std;

// Function Definitions
void swap(int& x, int& y)
{
	cout << "x initialised to = " << x << ", y initialised to = " << y << endl;
	//temp is assigned to X e.g. referenced to a
	int temp = *x;
	//x is assigned to y e.g. referenced to b
	*x = *y;
	//y is assigned to temp e.g. referenced to a
	*y = temp;
	cout << "x changed to = " << x << ", y changed to = " << y << endl;
};

/* Main function */
int main(void)
{
	int a = 3, b = 4;
	cout << "Before function call, a = " << a << ", b  = " << b << endl;
	swap(a, b);
	cout << "After  function call, a = " << a << ", b = " << b << endl;
	system("pause");
	return 0;
}

