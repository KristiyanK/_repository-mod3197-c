#include "Header.h"

// Function to evaluate Postfix expression and return output
string ConvertToPostfix(string str)
{
	//Declaring a Stack to hold operators. 
	stack<char> S;
	string postfixString = ""; //Initialize postfix as empty string.
	for (unsigned int i = 0; i< str.length(); i++) {

		// If character is operator, pop two elements from stack, perform operation and push the result back. 
		if (IsOperator(str[i]))
		{
			while (!S.empty() && S.top() != '(' && HasHigherPrecedence(S.top(), str[i]))
			{
				postfixString += S.top();
				S.pop();
			}
			S.push(str[i]);
		}
		// Else if character is an operand
		else if (IsOperand(str[i]))
		{
			postfixString += str[i];
		}

		else if (str[i] == '(')
		{
			S.push(str[i]);
		}

		else if (str[i] == ')')
		{
			while (!S.empty() && S.top() != '(') {
				postfixString += S.top();
				S.pop();
			}
			S.pop();
		}
	}

	while (!S.empty()) {
		postfixString += S.top();
		S.pop();
	}

	return postfixString;
}

// Checks if input is letter or digit. Only Single characters are legal.
bool IsOperand(char C)
{
	if (C >= '0' && C <= '9') return true;
	if (C >= 'a' && C <= 'z') return true;
	if (C >= 'A' && C <= 'Z') return true;
	return false;
}

// Function to verify whether a character is operator symbol or not. 
bool IsOperator(char C)
{
	if (C == '+' || C == '-' || C == '*' || C == '/')
		return true;

	return false;
}

// Function to verify if association is right. 
bool IsRightAssociative(char op)
{
	return false;
}

//Calculates the weight of an operator.
int precedence (char op)
{
	int weight = -1;
	switch (op)
	{
	case '+':
	case '-':
		weight = 1;
		break;
	case '*':
	case '/':
		weight = 2;
	}
	return weight;
}

// Function to verify whether an operator has higher precedence over other
int HasHigherPrecedence(char op1, char op2)
{
	int op1Weight = precedence(op1);
	int op2Weight = precedence(op2);

	// If operators have equal precedence, return true if they are left associative. 
	// return false, if right associative. 
	// if operator is left-associative, left one should be given priority. 
	if (op1Weight == op2Weight)
	{
		if (IsRightAssociative(op1)) return false;
		else return true;
	}
	return op1Weight > op2Weight ? true : false;

}

//Function to capture user's input
int Interface::GetInput(int userInput) 
{
	//std::cin >> userInput; //removed due to unpredictability
	return userInput;
}

void Interface::MenuConstructor(string option) 
{
	std::cout << option;
}

Interface::~Interface(void) 
{
	cout << "Object is being deleted" << endl;
}