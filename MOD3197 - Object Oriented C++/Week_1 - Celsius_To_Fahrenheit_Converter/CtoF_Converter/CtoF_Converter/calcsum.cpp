/* calcsum.c */
#include <iostream>

#pragma region Global variables
float a, answer;
#pragma endregion

float timestwo( float val )
{
   float CelsiusValue;
   CelsiusValue = (a - 32) * 5 / 9;
   return CelsiusValue;
};

/* Main function */
int main(void)
{
   std::cout << "Please enter the desired Fahrenheit value to be converted into Celsius> "; 
   std::cin >> a;
   answer = timestwo(a);
   std::cout << "\n" << a << " Fahrenheit is " << answer << " in Celsius." <<"\n";
   system("PAUSE");
   return 0;
}